package okocfg24

import (
	"fmt"
	"os"
	"strings"
	"sync"
	"time"

	yaml "gopkg.in/yaml.v3" //Did that to stop the red squigglies from my editor.
)

type cfgMapType map[string]string

// Read the config file and return configuration data
func readYaml() map[string]cfgMapType {

	loopCount := 0

	//Get the cfg file data and convert to string
	cfgMap := make(map[string]cfgMapType)
	data, err := os.ReadFile("cfg.yaml")
	if err != nil {
		panic(err.Error())
		// handle error
	}

	//Split different configs
	dSplit := strings.Split(string(data), "...")

	//Iterate over the different configs; put them in maps that will be used to retrieve those configs
	for _, doc := range dSplit {
		valmap := make(cfgMapType)
		if decErr := yaml.Unmarshal([]byte(doc), valmap); decErr != nil {
			panic(decErr.Error())
		}

		//Put the configs in the final map, with the process name being used as a key for the valmap, with the value being the config for the titular process
		cfgMap[valmap["process"]] = valmap
		//fmt.Printf("\r Loop %d full val: %v\n", loopCount, cfgMap)
		loopCount++
	}

	return cfgMap
}

// ========================================================================================
// GetDatPuller returns a generator that allows the user to poll the existing configuration
func GetDatPuller() func(string) map[string]string {
	dat := make(map[string]cfgMapType)
	var mtx sync.Mutex

	//First read to read the data
	dat = readYaml()

	//Break off a goroutine that keeps the config structure up to date
	go func() {
		for {
			mtx.Lock()
			dat = readYaml()
			mtx.Unlock()
			fmt.Print("\nRead Yaml complete ================================\n\n")
			time.Sleep(1 * time.Second)
		}
	}()

	//Return function that exposes a map of key/value string pairs that represent the configuration options
	return func(targ string) map[string]string {

		//TODO: Make this lock for less time
		mtx.Lock()
		if targ == "" {
			fmt.Println("Asking for empty string in GetDatPuller.")
		}

		if _, check := dat[targ]; check { //Handle returning the configuration in a map============

			/*
				fmt.Print("\nAsking for: ", targ, "\n\n")

				fmt.Print("\n\n\n===================WINDUMP\n")
				for k, v := range dat {
					fmt.Printf("K: %s, V: %s===\n", k, v)
				}

				fmt.Print("\n\n\n===================WINDUMP FIN\n")
			*/
			mtx.Unlock()
			return dat[targ]
		} else { //Handle missing the data ========================================================

			/*
				fmt.Print("\n\n\n===================FAILDUMP\n")

				fmt.Printf("\n\nCouldn't find targ for lookup. \nTarg was %s\n\n", targ)

				for k, v := range dat {
					fmt.Printf("K: %s, V: %s===\n", k, v)
				}

				fmt.Print("\n\n\n===================FAILDUMP FIN\n")
			*/
			mtx.Unlock()

			return dat[targ]
		}
	}
}
